# Vyklade's Reaper Pathfiner 2e Class

This is a small module containing the entirety of my [Reaper Homebrew Class](https://scribe.pf2.tools/v/5mOhtZcQc) for Pathfiner 2nd Edition. Complete with all class features, 48 feats, 5 spells and a handful of Macros to make the playing experience as enjoyable as possible.

You can also find my [Book of Homebrew Module](https://github.com/yoshisman8/Vyk-Homebrew) which contains some of my Homebrew Archetypes all in one module!

Icons are from Final Fantasy 14 © Square Enix.

## Installation

To Install, simply copy this URL and paste it in "Add-On Modules" > "Install Module" > "Manifest URL". Then click Install.

`https://gitlab.com/yoshisman8/pf2e-reaper/-/raw/main/module.json`

